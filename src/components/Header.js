import React, { Component } from 'react';
import { Nav, Navbar, NavItem, Image } from 'react-bootstrap';
import AuthActions from '../actions/AuthActions';
import AuthStore from '../stores/AuthStore';
import { Link } from 'react-router';
import '../styles/Header.css';

class HeaderComponent extends Component {

  constructor() {
    super();
    var user = JSON.parse(AuthStore.getUser());
    var authenticated = AuthStore.isAuthenticated();
    var profileImage = authenticated ? user.picture : null;
    this.state = {
      authenticated: authenticated,
      profileImage: profileImage
    };
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  login() {
    // We can call the show method from Auth0Lock,
    // which is passed down as a prop, to allow
    // the user to log in
    this.props.lock.show((err, profile, token) => {
      if (err) {
        alert(err);
        return;
      }
      AuthActions.logUserIn(profile, token);
      this.setState({
        authenticated: true,
        profileImage: profile.picture
      });
    });
  }

  logout() {
    AuthActions.logUserOut();
    this.setState({
      authenticated: false,
      profileImage: null
    });
  }

  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>
              React Contacts
            </Link>
          </Navbar.Brand>
        </Navbar.Header>
        { this.state.profileImage ? (
          <Image src={this.state.profileImage} circle />
        ) : null }
        <Nav>
          { !this.state.authenticated ? (
            <NavItem onClick={this.login}>Login</NavItem>
          ) : (
            <NavItem onClick={this.logout}>Logout</NavItem>
          )}
        </Nav>
      </Navbar>
    );
  }
}

export default HeaderComponent;
