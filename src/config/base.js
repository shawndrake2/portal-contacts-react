'use strict';

// Settings configured here will be merged into the final config object.
export default {
  apiPort: 4000
}
